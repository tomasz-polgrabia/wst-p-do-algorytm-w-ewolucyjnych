func1 <- function(x,y) {
    diffX <- x
    diffY <- y
    value <- -0.2*diffX*diffX - 0.5*diffY*diffY + 4
}


min = -0.1
max = 0.1
n = 10000

x <- array(1:1, dim=c(n,2))
x[1,] <- rnorm(2,0, 0.1)
sumBest <- func1(x[1,1],x[1,2])

best = x[1,]

for (i in 2:n) {
    x[i,1] <- best[1] + rnorm(1, 0,0.1)
    x[i,2] <- best[2] + rnorm(1, 0, 0.1)
    currSum <- func1(x[i,1],x[i,2])
    if (sumBest < currSum) {
        sumBest <- currSum
        best = x[i,]
    }
}

plot(x[,1],x[,2], col="blue", pch=0, cex=0.1, xlabel="x", ylabel="y")


