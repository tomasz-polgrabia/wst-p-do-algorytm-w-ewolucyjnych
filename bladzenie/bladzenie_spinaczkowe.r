min = -0.01
max = 0.01
n = 1000

x <- array(1:1, dim=c(n,2))
x[1,] <- runif(2)
sumBest <- sum(x[1,])

best = x[1,]

for (i in 2:n) {
    x[i,] <- best + runif(2,min,max)
    currSum <- sum(x[i,])
    if (sumBest < currSum) {
        sumBest <- currSum
        best = x[i,]
    }
}

plot.new()
points(x[,1],x[,2], col="blue", pch=0, cex=0.1)


