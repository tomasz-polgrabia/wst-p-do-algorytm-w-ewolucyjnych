library(mvtnorm)
library(ellipse)

func <- function(x) sin(x[1])+cos(x[2])
func2 <- function(x,y) func(c(x,y))
func3 <- Vectorize(func2)

minx <- -10
miny <- -10
maxx <- 10
maxy <- 10

dim <- 2
rounds <- 20
epsilon <- 0.00000000001
sd <- 0.5

populationSize <- 20
selectedSize <- 5

initialMean <- function() {
#    runif(dim,minx,maxx)
    c(-5,5)
}

initialDistr <- function() {
    diag(rep(sd^2,each=dim))
}

select <- function(population) {
    values <- unlist(lapply(population,func))
    ord <- order(values,decreasing=TRUE)
    orderedValues <- values[ord]
    orderedPopulation <- population[ord]
    return(orderedPopulation[1:selectedSize])
}

update <- function(population,mean,distribution) {
    n <- length(population)
    values <- unlist(lapply(population,func))
    m <- min(values)
    s <- sum(values - m)

    if (s >= epsilon) {
        weights <- (values - m) / s
    } else {
        weights <- rep(1.0 / n, n)
    }

#    print("update")
#    print(values)
#    print(m)
#    print(population)
#    print(weights)

    newMean <- 1:dim

    for (j in 1:dim) {
        newMean[j] <- 0
        for (i in 1:n) {
            newMean[j] <- newMean[j] + weights[i]*population[[i]][j]
        }
    }

#    print(newMean)

    diagonal <- 1:dim
    for (j in 1:dim) {
        diagonal[j] <- 0
        for (i in 1:n) {
            diagonal[j] <- diagonal[j] + weights[i] * (population[[i]][j] - newMean[j])^2
        }
    }

    newDistribution <- diag(diagonal)

    list(newMean,newDistribution)

}

extract <- function(vec, idx) {
    vec[idx]
}

eda <- function() {

    history <- vector("list", rounds)
    H <- vector("list", rounds)

    m <- initialMean()
    C <- initialDistr()

    pair <- list(m,C)
#    print(pair)

    H[[1]] <- pair

    points <- list()

    for (t in 2:rounds) {

        dev.hold()

        plot.new()

        pair <- H[[t-1]]

        m <- pair[[1]]
        C <- pair[[2]]

#        print(C)

        populationMatrix <- rmvnorm(populationSize,mean=m,sigma=C)
        population <- as.list(data.frame(t(populationMatrix)))

        points <- c(points,population)

        history[[t]] <- population

        selected <- select(population)

        pair <- update(selected,pair[[1]],pair[[2]])

#        print(pair[[1]])
#        print(diag(pair[[2]]))

        H[[t]] <- pair

        extractX <- function(x) extract(x,1)
        extractY <- function(x) extract(x,2)
        x <- unlist(lapply(population,extractX))
        y <- unlist(lapply(population,extractY))

        plot(x,y,xlim=c(minx,maxx),ylim=c(miny,maxy))
        title(paste(c("Runda nr ",t),collapse=" "))

        dev.flush()

#        Sys.sleep(1)

    }

    dev.hold()
    extractX <- function(x) extract(x,1)
    extractY <- function(x) extract(x,2)
    x <- unlist(lapply(points,extractX))
    y <- unlist(lapply(points,extractY))

    plot(x,y,xlim=c(-6,-4),ylim=c(5,7))
    title("Historia")

    x <- seq(minx,maxx,by=0.1)
    y <- seq(miny,maxy,by=0.1)
    z <- outer(x,y,func3)

    contour(x,y,z,add=TRUE,nlevels=100)

    for (i in 1:rounds) {
        points <- ellipse(H[[i]][[2]],centre=H[[i]][[1]])
        lines(points[,1],points[,2],col="red")
        points(H[[i]][[1]][1],H[[i]][[1]][2],col="blue")
    }

    dev.flush()

#    return(points)

}
