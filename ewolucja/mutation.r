limit_minx <- -10
limit_miny <- -10
limit_maxx <- 10
limit_maxy <- 10

dim <- 2
sd <- 1.0
epsilon <- 0.1

rounds <- 10000
populationSize <- 1000

func <- function(x) 1

mutate <- function(vec) {
    vec + rnorm(dim, 0, sd)
}

mutation <- function(population) {
    lapply(population, mutate)
}

restrict <- function(vec) {
    vec2 <- c(min(limit_maxx, max(limit_minx, vec[1])),
      min(limit_maxy, max(limit_miny, vec[2])))
    return(vec2)
}

restriction <- function(population) {
    lapply(population, restrict)
}

reproduct <- function(population) {
    n <- length(population)
    values <- unlist(lapply(population,func))
    m <- min(values)
    s <- sum(values - m)

    if (s < epsilon) {
        weights <- 1:n
        for (i in 1:n) {
            weights[i] <- 1.0 / n
        }
    }
    else {
        weights <- (values - m) / s
    }


    cSum <- 0
    r <- runif(1,0,1)

    for (i in 1:n) {
        cSum <- cSum + weights[i]
        if (cSum >= r) {
            return(i)
        }
    }

    return(0)

}

reproduction <- function(population) {
    n <- length(population)
    choosen <- 1:n

    for (i in 1:n) {
        choosen[i] <- reproduct(population)
    }
    return(population[choosen])
}

getInitial <- function() {
    population <- vector("list", populationSize)

    for (i in 1:populationSize) {
        vec <- 1:2
        vec[1] <- runif(1, min=limit_minx, max=limit_maxx)
        vec[2] <- runif(1, min=limit_miny, max=limit_maxy)
        population[[i]] <- vec
    }

    return(population)

}

extract <- function(vec, idx) {
    vec[idx]
}

randomPoint <- function(population) {
    n <- length(population)
    r <- round(runif(1, min=1,max=n))
    return(population[[r]])
}

factor <- 1.0

mutateBinom <- function(vec1,vec2) {
    n <- length(vec1)
    mask <- rbinom(n,1,0.5)
    vec1*mask+vec2*(1-mask)
}

tournament <- function(vec1,vec2) {
    val <- func(vec1)
    val2 <- func(vec2)

    if (val > val2) {
        return(vec1)
    } else {
        return(vec2)
    }
}

simulate <- function() {
    H <- vector("list", rounds)
    population <- getInitial()

    H[[1]] <- population

    for (t in 2:rounds) {
        print(paste(c("Populacja nr", t), collapse=" "))
        basePopulation <- H[[t-1]]

        newPopulation <- basePopulation

        for (i in 1:populationSize) {

            r1 <- randomPoint(population)
            r2 <- randomPoint(population)
            r3 <- randomPoint(population)

            rRes <- r1 + factor*(r2 - r3)
            mutated <- mutateBinom(basePopulation[[i]],rRes)

            newPopulation[[i]] <- tournament(basePopulation[[i]],mutated)

            # tak możemy bo stała wartość brak turnieju

        }

        restricted <- restriction(newPopulation)

        H[[t]] <- restricted

        extractX <- function(vec) extract(vec, 1)
        extractY <- function(vec) extract(vec, 2)

        x <- unlist(lapply(restricted, extractX))
        y <- unlist(lapply(restricted, extractY))

        dev.hold()
        plot(x,y, xlim=c(limit_minx, limit_maxx),
             ylim=c(limit_miny, limit_maxy))
        title(paste(c("Populacja nr", t), collapse=" "))
        dev.flush()




    }

}

