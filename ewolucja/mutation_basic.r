    mutation <- function(vec,pair) {

        return (c((vec[1]+pair[1])/2.0, (vec[2]+pair[2])/2.0))
    }

    select <- function(population) {
        n <-dim(population)
        c <- sample(1:n[1],1)
        return (population[c,])
    }


    # main <- function() {
    n <- 1000
    u <- 100
    P <- array (1:1, dim=c(n,u,2))

    for (i in 1:u) {
        P[1,i,] <- runif(2)
    }

    for (t in 2:n) {
        print (t)
        O <- array(1:1, dim=c(u,2))
        for (i in 1:u) {
            O[i,] <- mutation(P[t-1,i,],select(P[t-1,,])) # skrzyżowany osobnik
            P[t,,] = O

#            write.csv(O,file = paste("plot", as.character(t), ".csv", sep=""))

#            png(paste("plot", as.character(t), ".png", sep=""))
#            plot(O[,1],O[,2],xlab="x", ylab="y")
#            points(c(O[1,1]), c(O[1,2]), pch=21, bg="red")
#            dev.off()

        }
    }

# }
