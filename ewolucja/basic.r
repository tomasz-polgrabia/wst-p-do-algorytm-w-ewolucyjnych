# basic.main <- function() {

    n <- 1000
    H <- array(1:1, dim=c(n,2))

    H[1,] <- runif(2)
    stop <- FALSE

    for (i in 2:n) {
        count <- i
        xId <- rbinom(1,count,0.5)
        x <- H[xId,]
        H[i,] <- rnorm(2, 0, 0.01) + H[i-1,]
    }

    png("rplot.png")
    plot(H[,1],H[,2],xlab="x", ylab="y")
    points(c(H[1,1]), c(H[1,2]), pch=21, bg="red")
    dev.off()
# }
