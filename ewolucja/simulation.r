rounds <- 30
H <- vector("list", rounds)
epsilon <- 0.01

func <- function(x) x^2

chooseProportional <- function(population) {
    n <- length(population)
    values <- unlist(lapply(population, func))
    m <- min(values)
    s <- sum(values-m)
    weights <- values

    if (s <= epsilon) {
        weights <- values
        for (i in 1:n) {
            weights[i] <- 1.0 / n
        }
    }
    else {
        weights <- (weights - m) / s
    }
    r <- runif(1, 0, 1)

    sum <- 0.0
    for (i in 1:n) {
        sum <- sum + weights[i]
        if (sum >= r) {
            return(i)
        }
    }

    return(1)

}

chooseAll <- function(population) {
    n <- length(population) 
    vec <- 1:n
    for (i in 1:n) {
        vec[i] <- chooseProportional(population)
    }

    return(vec)
}

bestId <- function(population) {
    values <- unlist(lapply(population, func))
    o <- order(values)
    n <- length(population)
    return(o[n])
}

start <- function() {

    sum <- 1:10
    sum <- sum - sum
    repeats <- 100

    left <- 1:rounds
    for (j in 1:repeats) {

        population <- runif(10, 0, 5)
        for (i in 1:rounds) {

            population <- sort(population)
            diffs <- diff(population)
            diffs <- abs(diffs)

            diffs <- diffs[diffs >= epsilon]
            l <- length(diffs)
            left[i] <- l

            print(paste(c("Runda", i, "Liczba roznych", l),collapse=" "))
            reproducted <- chooseAll(population)
            newPopulation <- population[reproducted]
            best <- bestId(population)
            newPopulation[best] <- population[best]
            population <- newPopulation
            H[[i]] <- newPopulation
        }

        sum <- sum + left
   }

    sum <- sum / repeats

    plot(1:rounds,left)

}
